#!/bin/bash

exec 1> /tmp/keepalived_state.out
exec 2>&1

TYPE=$1
NAME=$2
STATE=$3
output_dir=/var/run/keepalived
output_file=${output_dir}/state
toggle_dir=/usr/local/etc/keepalived_toggle.d

mkdir -p ${output_dir}

case ${STATE} in
  MASTER|BACKUP|FAULT|STOPPED)
    echo "${STATE}" >${output_file}
    echo "$(basename $0) changing to ${STATE} (args are : $*)"
    ;;
  *)
    echo "unknown state" >${output_file}
    echo "$(basename $0) changing to ${STATE} (args are : $*)"
    exit 1
    ;;
esac

case ${STATE} in
  BACKUP|FAULT|STOPPED)
    toggle_action="stop"
    ;;
  MASTER)
    toggle_action="start"
    ;;
esac

echo "running toggle scripts"
for toggle_script in $(ls ${toggle_dir}/* 2>/dev/null)
do
  echo "  running '${toggle_script} ${toggle_action}'"
  ${toggle_script} ${toggle_action}
done
echo "toggle scripts played"
